package innerClasses;

public class StaticNestedClass {
	int num = 10;
	static String name = "rahul";
	static class MyInner{
		public void doStuff(){
			System.out.println("do stuff");
			//static nested class cannot access the instance variables of outer class
//			System.out.println("num: "+num);
			//static nested class can access the static members of outer class
			System.out.println("name: "+name);
		}
	}
	public static void main(String[] args) {
		StaticNestedClass.MyInner mi = new StaticNestedClass.MyInner();
		mi.doStuff();
	}
}
