package innerClasses;

public class MethodLocalInnerClass {
	public static void main(String[] args) {
		Outer2 outer = new Outer2();
		outer.doStuff();
	}
}

class Outer2{
	
	int num = 10;
	
	public void doStuff(){
		final String str = "hello";
		
		//defining method local inner class
		class Inner2{
			public void doStuff2(){
				System.out.println("Inside inner class' doStuff2 method");
				//only final local variables of the enclosing method can be accessed iside the inner calss
				System.out.println(str);
				//inner class can access the outer class instance variables
				System.out.println("num " + num);
			}
		}
		//instantiating method local inner class
		Inner2 inner2 = new Inner2();
		inner2.doStuff2();
	}
	
}
