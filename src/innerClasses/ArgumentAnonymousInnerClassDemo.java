package innerClasses;

public class ArgumentAnonymousInnerClassDemo {
	public static void main(String[] args) {
		B b = new B();
		//creating interface's implementer in the method argument
		b.go(new A(){
			public void doStuff(){
				System.out.println("doing stuff");
			}
		});
	}
}

interface A{
	void doStuff();
}

class B{
	public void go(A a){
		a.doStuff();
	}
}
