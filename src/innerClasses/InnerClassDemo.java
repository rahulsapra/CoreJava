package innerClasses;

public class InnerClassDemo {

	public static void main(String[] args) {
		Outer ou = new Outer();
		ou.callInner();
		Outer.Inner inner = ou.new Inner();
		inner.seeOuter();
	}
	
}

class Outer{

	int num = 10;
	
	public void callInner(){
		Inner inner = new Inner();
		inner.seeOuter();
	}
	
	
	class Inner{
		public void seeOuter(){
			System.out.println(num);
		}
	}
	
}


