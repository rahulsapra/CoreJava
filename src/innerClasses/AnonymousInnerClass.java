package innerClasses;

public class AnonymousInnerClass {
	public static void main(String[] args) {
		Popcorn popcorn = new Popcorn(){
			public void pop(){
				System.out.println("anonymous popcorn");
			}
			public void sizzle(){
				System.out.println("sizzle");
			}
		};
		popcorn.pop();
		//we cannot call pocorn.sizzle because sizzle method is not present in the actual Popcorn class
//		popcorn.sizzle();
		
		//creating implementer of an interface
		MyInterface mi = new MyInterface() {
			
			@Override
			public void doSomething() {
				// TODO Auto-generated method stub
				System.out.println("doing something");
			}
		};
		mi.doSomething();
		
	}
}

class Popcorn{
	public void pop(){
		System.out.println("pop");
	}
}


interface MyInterface{
	void doSomething();
}
