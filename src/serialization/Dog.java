package serialization;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Dog implements Serializable {
	private String name;
	private String type;
	
	public Dog(){}
	
	public Dog(String name, String type){
		this.name = name;
		this.type = type;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}
	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	public static void main(String[] args) {
		Dog dog = new Dog();
		dog.setName("doggy");
		dog.setType("labra");
		
		try{
			File file = new File("E:/code jam/output.txt");
			FileOutputStream fos = new FileOutputStream(file);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(dog);
			dog.setName("tiger");
			//by default os restores the references ofojects it has already writtern.
			//So if we change dog and need to write the updated dog. we can't without either
			//resetting os Or closing the os and creating a new os
			//in case of resetthe updated dog is written. But when we try to read the object, 
			//the previous version of th object is retrieved
			os.reset();
			os.writeObject(dog);
			
			FileInputStream fis = new FileInputStream(file);
			ObjectInputStream ois = new ObjectInputStream(fis);
			Dog d = (Dog) ois.readObject();
			System.out.println(d.getName() + " "+d.getType());
			
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
	
}
