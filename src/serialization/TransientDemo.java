package serialization;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class TransientDemo {
	public static void main(String[] args) {
		B b = new B();
		b.setNum(5);
		A a = new A(b);
		try{
			File file = new File("E:/code jam/output.txt");
			FileOutputStream fos = new FileOutputStream(file);
			ObjectOutputStream os = new ObjectOutputStream(fos);
			os.writeObject(a);

			FileInputStream fis = new FileInputStream(file);
			ObjectInputStream is = new ObjectInputStream(fis);
			A a1 = (A)is.readObject();
			System.out.println(a1);
			System.out.println(a1.getB());
//			A a2 = new A();
//			a2 = is.readObject();
			System.out.println(a1.getB().getNum());

		}
		catch(Exception e){
			e.printStackTrace();
		}

	}
}

class A implements Serializable {
	private transient B b;

	public A() {}

	public A(B b) {
		this.b = b;
	}

	private void writeObject(ObjectOutputStream os){
		try{
			os.defaultWriteObject();
			os.writeInt(b.getNum());
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}


	private void readObject(ObjectInputStream is){
		try{
			is.defaultReadObject();
			b = new B();
			this.b = new B(is.readInt());
		}
		catch(Exception e){

		}
	}



	/**
	 * @return the b
	 */
	public B getB() {
		return b;
	}

	/**
	 * @param b the b to set
	 */
	public void setB(B b) {
		this.b = b;
	}



}


class B {
	private int num;

	public B(){}
	
	public B(int num){
		this.num = num;
	}
	
	
	/**
	 * @return the num
	 */
	public int getNum() {
		return num;
	}

	/**
	 * @param num the num to set
	 */
	public void setNum(int num) {
		this.num = num;
	}


}
