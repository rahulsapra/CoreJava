package collections;
import java.util.Arrays;


public class ArraySearchDemo {
	public static void main(String[] args) {
		int[] a = {1,2,3,11,8,6,5,7};
		Arrays.sort(a);
//		System.out.println(Arrays.binarySearch(a, 10));
		System.out.println(Arrays.binarySearch(a, 4));
	}
}
