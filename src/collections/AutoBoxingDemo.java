package collections;

public class AutoBoxingDemo {
	static Integer x;
	public static void main(String[] args) {
		doStuff(x);
	}
	
	public static void doStuff(int x){
		System.out.println(10 + x);
	}
	//running the program throws NullPointerException as x doesn't refer to an integer and can't be
	// unboxed to compute the result in the method doStuff
	
}
