package collections;
import java.util.SortedSet;
import java.util.TreeSet;


public class BackedCollectionDemo {
	public static void main(String[] args) {
		TreeSet<Integer> treeSet = new TreeSet<>();
		treeSet.add(1);
		treeSet.add(2);
		treeSet.add(3);
		treeSet.add(4);
		treeSet.add(5);
		treeSet.add(6);
		treeSet.add(7);
		treeSet.add(8);
		treeSet.add(9);
		treeSet.add(10);
		
		SortedSet<Integer> t1 = treeSet.headSet(5);
		System.out.println("t1: "+t1);
		SortedSet<Integer> t2 = treeSet.tailSet(5);
		System.out.println("t2: "+t2);
		SortedSet<Integer> t3 = treeSet.subSet(2, 8);
		System.out.println("t3: "+t3);
		
		SortedSet<Integer> t4 = treeSet.headSet(5, true);
		System.out.println("t4: "+t4);
		//similar methods are there for TreeMap	
		
	}
}
