package collections;
import java.util.PriorityQueue;


public class PriorityQueueDemo {
	public static void main(String[] args) {
		PriorityQueue<Integer> pq = new PriorityQueue<>();
		//inserting elements in the queue
		pq.offer(1);
		pq.offer(2);
		pq.offer(3);
		pq.offer(4);
		pq.offer(5);
		pq.offer(6);
		pq.offer(7);
		//getting the higher priority element
		System.out.println("Peek: "+pq.peek());
		System.out.println("pq.size: "+pq.size());
		int size = pq.size();
		System.out.println("size: "+size);
		for(int i=0; i<size; i++){
			System.out.print(pq.poll() + " ");
		}
	}
}
