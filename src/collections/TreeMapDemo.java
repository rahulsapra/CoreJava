package collections;
import java.util.TreeMap;


public class TreeMapDemo {
	public static void main(String[] args) {
		TreeMap<Integer, Integer> treeMap = new TreeMap<>();
		treeMap.put(1, 1);
		treeMap.put(2, 2);
		treeMap.put(3, 3);
		treeMap.put(4, 4);
		treeMap.put(5, 5);
		treeMap.put(6, 6);
		treeMap.put(7, 7);
		treeMap.put(8, 8);
		treeMap.put(9, 9);
		
		System.out.println(treeMap);
		
		System.out.println(treeMap.floorEntry(6));
		System.out.println(treeMap.lowerEntry(6));
		System.out.println(treeMap.higherEntry(3));
		System.out.println(treeMap.ceilingEntry(3));
		
		
		System.out.println(treeMap.floorKey(6));
		System.out.println(treeMap.lowerKey(6));
		System.out.println(treeMap.higherKey(3));
		System.out.println(treeMap.ceilingKey(3));
		
		
		treeMap.pollFirstEntry();
		System.out.println(treeMap);
		
		treeMap.pollLastEntry();
		System.out.println(treeMap);
		
		//returns headMap
		System.out.println(treeMap.headMap(5));
		
		//returns tailMap
		System.out.println(treeMap.tailMap(5));
		
	}
}
