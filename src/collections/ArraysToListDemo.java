package collections;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;


public class ArraysToListDemo {
	public static void main(String[] args) {
		String[] str = {"a", "b", "c", "d"};
		List strings = Arrays.asList(str);
		Iterator<String> it = strings.iterator();
		while(it.hasNext()){
			System.out.println(it.next());
		}
		
		strings.set(3, "new");
		for(String s : str){
			System.out.print(s + ", ");
		}
		
	}
}
