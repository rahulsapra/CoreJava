package collections;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class ComparableDemo {
	public static void main(String[] args) {
		List<Person> persons = new ArrayList<>();
		persons.add(new Person(1, "a"));
		persons.add(new Person(2, "b"));
		persons.add(new Person(3, "c"));
		persons.add(new Person(5,"a"));
		persons.add(new Person(6,"a"));
		persons.add(new Person(4,"d"));
		System.out.println("unsorted: "+persons);
		Collections.sort(persons);
		System.out.println("sorted: "+persons);
		
		//sorting the persons by name
		Collections.sort(persons, new CustomComparator());
		System.out.println("sorted by name: "+persons);
		
	}
	
}

class Person implements Comparable<Person>{
	int age;
	String name;
	
	public Person(){
		this.age = 0;
		this.name = "";
	}
	
	public Person(int age, String name){
		this.age = age;
		this.name = name;
	}
	
	
	@Override
	public String toString(){
		return "name: "+name+" age: "+age;
	}
	
	@Override
	public int compareTo(Person p) {
		if(this.age < p.age){
			return -1;
		}
		else if(this.age == p.age){
			return 0;
		}
		else{
			return 1;
		}
	}
	
}

/**
 * comparator to sort the person object on the basis of name
 * @author rahul
 *
 */
class CustomComparator implements Comparator<Person>{

	@Override
	public int compare(Person o1, Person o2) {
		return o1.name.compareTo(o2.name);
	}
	
}
