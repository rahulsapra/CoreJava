package collections;
import java.util.Iterator;
import java.util.TreeSet;


public class TreeSetDemo {
	public static void main(String[] args) {
		TreeSet<Integer> treeSet = new TreeSet<>();
		treeSet.add(1);
		treeSet.add(4);
		treeSet.add(2);
		treeSet.add(5);
		treeSet.add(6);
		treeSet.add(7);
		treeSet.add(8);
		treeSet.add(9);
		
		Iterator< Integer> iterator = treeSet.iterator();
		while(iterator.hasNext()){
			System.out.println(iterator.next());
		}
		
		//some methods of treeset inherited from navigableset
		//lower: returns element lower than the given value
		System.out.println("treeSet.lower(8): "+treeSet.lower(8));
		//floor: returns element lower or equal to given value
		System.out.println("treeSet.floor(8): "+treeSet.floor(8));
		//higher: returns element higher than the given value
		System.out.println("treeSet.higher(6): "+treeSet.higher(6));
		//ceiling: returns element higher or equal to the given value
		System.out.println("treeSet.ceiling(6): "+treeSet.ceiling(6));
		
		
		//pollFirst
		treeSet.pollFirst();
		System.out.println(treeSet);
		
		//pollLast
		treeSet.pollLast();
		System.out.println(treeSet);
		
		//returns navigableSet
		System.out.println(treeSet.descendingSet());
		
		//returns headset
		System.out.println(treeSet.headSet(5));
		
		//returns tailset
		System.out.println(treeSet.tailSet(5));
		
		
	}
}
